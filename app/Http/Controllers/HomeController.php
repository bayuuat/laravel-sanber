<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // public function index(){
    //     return view('home');
    // }

    // public function master(){
    //     return view('adminlte.master');
    // }

    public function item(){
        return view('items.index');
    }

    public function data(){
        return view('items.data');
    }
}
