<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create(){
        return view('post.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            "judul" => 'required | unique:pertanyaan',
            "isi" => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Post Berhasil Disimpan!');
    }

    public function index(){
        $post = DB::table('pertanyaan')->get();
        return view('post.index', compact('post'));
    }

    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('post.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request){
        $post = DB::table('pertanyaan')->where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Post Berhasil Update!');
    }

    public function destroy($id){
        $post = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Post Berhasil Dihapus!');
    }
}
