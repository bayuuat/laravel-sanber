<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/send" method="POST">
        @csrf
            <p>First Name:</p>
            <input type="text" name="fname"/>
            <p>Last Name:</p>
            <input type="text" name="lname"/>
            <p>Gender:</p>
            <input type="radio" name="gender"/>
            <label>Male</label><br />
            <input type="radio" name="gender"/>
            <label>Female</label>
            <p>Nationality:</p>
            <select>
                <option>Indonesian</option>
                <option>American</option>
                <option>Singaporean</option>
                <option>Italian</option>
            </select>
            <p>Language Spoken:</p>
            <input type="checkbox" />
            <label>Bahasa Indonesia</label><br />
            <input type="checkbox" />
            <label>English</label><br />
            <input type="checkbox" />
            <label>Other</label><br />
            <p>Bio:</p>
            <textarea rows="10" cols="25"></textarea><br />
            <input type="submit" value="Submit" />
        </form>
    </body>
</html>
