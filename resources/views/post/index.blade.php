@extends('adminlte.master')

@section('content')
    <div class="mx-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Bordered Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              @if (session('success'))
                  <div class="alert alert-success">
                      {{session('success')}}
                  </div>
              @endif
              <a class="btn btn-dark mb-2" href='/pertanyaan/create'>+ Create New Post</a>
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($post as $key => $ipost)
                      <tr>
                          <td>{{$key + 1}}</td>
                          <td>{{$ipost->judul}}</td>
                          <td>{{$ipost->isi}}</td>
                          <td style="display: flex;">
                              <a href="/pertanyaan/{{$ipost->id}}" class="btn btn-info btn-sm">Show</a>
                              <a href="/pertanyaan/{{$ipost->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                              <form action="/pertanyaan/{{$ipost->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                              </form>
                          </td>
                      </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Post</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@endsection