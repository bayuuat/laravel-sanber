<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@item');
Route::get('/pertanyaan/create', 'PostController@create');
Route::post('/pertanyaan', 'PostController@store');
Route::get('/pertanyaan', 'PostController@index');
Route::get('/pertanyaan/{id}', 'PostController@show');
Route::put('/pertanyaan/{id}', 'PostController@update');
Route::delete('/pertanyaan/{id}', 'PostController@destroy');
Route::get('/pertanyaan/{id}/edit', 'PostController@edit');
// Route::get('/data-tables', 'HomeController@data');
// Route::get('/register', 'AuthController@form');
// Route::post('/send', 'AuthController@send');
// Route::get('/welcome', 'AuthController@welcome');
// Route::get('/master', 'HomeController@master');
